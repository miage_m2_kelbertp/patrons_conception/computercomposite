public class Component {
    protected int price;
    protected int consumption;

    public Component(int price, int consumption) {
        this.price = price;
        this.consumption = consumption;
    }

    public void display() {
        System.out.println("Component - " + this.getClass().getName() + " : ");
        System.out.println("\tPrice: " + price + "€");
        System.out.println("\tConsumption: " + consumption + "W");
    }
}
