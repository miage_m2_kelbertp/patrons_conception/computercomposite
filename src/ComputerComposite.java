import java.util.ArrayList;
import java.util.List;

public class ComputerComposite extends Component {
    private List<Component> components = new ArrayList<>();

    public ComputerComposite() {
        super(0, 0);
    }

    public ComputerComposite addComponent(Component component) {
        components.add(component);
        return this;
    }

    @Override
    public void display(){
        int totalPrice, totalComsommation;
        totalPrice = totalComsommation = 0;
        for (Component component : components) {
            component.display();
            totalPrice += component.price;
            totalComsommation += component.consumption;
        }
        System.out.println();
        System.out.println("Computer - " + this.getClass().getName() + " : ");
        System.out.println("\tPrice: " + totalPrice + "€");
        System.out.println("\tConsumption: " + totalComsommation + "W");
    }
}
