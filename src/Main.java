public class Main {
    public static void main(String[] args) {
        ComputerComposite computer = new ComputerComposite();
        computer.addComponent(new CPU(300, 100))
                .addComponent(new GPU(600, 200))
                .addComponent(new RAM(86, 50))
                .addComponent(new Screen(250, 50));

        computer.display();
    }
}